package org.xman.guava.file;


import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FilesTest {

    @Test
    public void testReadLines() {
        File file = new File("src/main/resources/lines.txt");

        List<String> expectedLines = Lists.newArrayList("The quick brown", "fox jumps over", "the lazy dog");
        try {
            List<String> readLines = Files.readLines(file, Charsets.UTF_8);
            assertThat(expectedLines, is(readLines));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
