package org.xman.guava.basic;


import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SplitterTest {
    private String sequence = "foo | bar | | | baz ";

    @Test
    public void simpleTest() {
        Splitter splitter = Splitter.on('|');
        Iterable<String> results = splitter.split(sequence);

        System.out.println(results);
    }

    @Test
    public void trimResultsTest() {
        Splitter splitter = Splitter.on('|').trimResults();
        Iterable<String> results = splitter.split(sequence);

        System.out.println(results);
    }

    @Test
    public void omitEmptyStringsTest() {
        Splitter splitter = Splitter.on('|').trimResults().omitEmptyStrings();
        Iterable<String> results = splitter.split(sequence);

        System.out.println(results);
    }


    @Test
    public void mapSplitterTest() {
        Map<String, String> testMap = Maps.newLinkedHashMap();
        testMap.put("Washington D.C", "Redskins");
        testMap.put("New York City", "Giants");
        testMap.put("Philadelphia", "Eagles");
        testMap.put("Dallas", "Cowboys");

        String startString = "Washington D.C=Redskins#New York City=Giants#Philadelphia=Eagles#Dallas=Cowboys";
        Splitter.MapSplitter mapSplitter = Splitter.on("#").withKeyValueSeparator("=");
        Map<String, String> splitMap = mapSplitter.split(startString);

        assertThat(testMap, is(splitMap));
    }
}
