package org.xman.guava.basic;


import com.google.common.base.CharMatcher;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CharMatcherTest {
    private final char ONE_SPACE = ' ';

    /**
     * 替换多余空格。
     */
    @Test
    public void testCollapseFrom() {
        String tabsAndSpaces = "String  with spaces and        tabs";
        String expected = "String with spaces and tabs";
        String scrubbed = CharMatcher.WHITESPACE.collapseFrom(tabsAndSpaces, ONE_SPACE);
        assertThat(scrubbed, is(expected));
    }

    /**
     * 替换多余空格，但忽略开头和结尾的空格。
     */
    @Test
    public void testTrimAndCollapseFrom() {
        String tabsAndSpaces = "  String with spaces and        tabs";
        String expected = "String with spaces and tabs";
        String scrubbed = CharMatcher.WHITESPACE.trimAndCollapseFrom(tabsAndSpaces, ONE_SPACE);
        assertThat(scrubbed, is(expected));
    }

    /**
     * 保留数字。
     */
    @Test
    public void testRetainFrom() {
        String lettersAndNumbers = "foo989yxbar234";
        String expected = "989234";
        String retained = CharMatcher.JAVA_DIGIT.retainFrom(lettersAndNumbers);
        assertThat(expected, is(retained));
    }

    /**
     * CharMatcher组合测试。
     */
    @Test
    public void testCombineCharMatcher() {
        String sequence = "foo989yxbar234 aa  bb  cc   ";
        CharMatcher cm = CharMatcher.JAVA_DIGIT.or(CharMatcher.WHITESPACE);

        // 匹配数字与空格的组合
        String result = cm.trimAndCollapseFrom(sequence, ONE_SPACE);
        String expected = "foo yxbar aa bb cc";
        assertThat(expected, is(result));
    }
}
