package org.xman.guava.basic;


import com.google.common.base.Charsets;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class CharsetTest {
    private String sequence = "foobarbaz";

    @Test
    public void TestJdkGetBytes1() {
        try {
            byte[] result = sequence.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            //This really can't happen UTF-8 must be supported
        }
    }

    @Test
    public void TestJdkGetBytes2() {
        // JDK7中新增加的StandardCharset工具类
        byte[] result = sequence.getBytes(StandardCharsets.UTF_8);
    }

    @Test
    public void TestGuavaGetBytes() {
        byte[] result = sequence.getBytes(Charsets.UTF_8);
    }


}
