package org.xman.guava.basic;


import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class JoinerTest {
    private static final String delimiter = "|";
    List<String> stringList;
    List<String> stringListWithNull;


    @Before
    public void before() {
        stringList = new ArrayList<>();
        stringList.add("one");
        stringList.add("two");
        stringList.add("three");
        stringList.add("four");

        stringListWithNull = new ArrayList<>(stringList);
        stringListWithNull.add(null);
        stringListWithNull.add("six");
        stringListWithNull.add(null);
        stringListWithNull.add("eight");
    }

    @Test
    public void skipNullTest() {
        String msg1 = Joiner.on(delimiter).skipNulls().join(stringListWithNull);
        System.out.println(msg1);

        Joiner stringJoiner = Joiner.on(delimiter).skipNulls();
        System.out.println(stringJoiner.join(stringList));
    }

    @Test
    public void useForNullTest() {

        Joiner j1 = Joiner.on(delimiter).useForNull("missing");
        System.out.println(j1.join(stringListWithNull));
    }

    @Test
    public void appendToTest() {
        StringBuilder stringBuilder = new StringBuilder();
        Joiner j1 = Joiner.on("|").skipNulls();

        // returns the StringBuilder instance with the values foo,bar,baz
        // appended with "|" delimiters
        j1.appendTo(stringBuilder, "foo", "bar", "baz");
        System.out.println(stringBuilder.toString());

        Joiner j2 = Joiner.on(";").useForNull("missing");
        // returns the FileWriter instance with the values
        // appended into it
        try {
            FileWriter fileWriter = new FileWriter(new File("appendToTest.txt"));
            j2.appendTo(fileWriter, stringListWithNull);
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void mapJoinerTest() {
        //Using LinkedHashMap so that the original order is preserved
        String expectedString = "Washington D.C=Redskins#" +
                "New York City=Giants#" +
                "Philadelphia=Eagles#" +
                "Dallas=Cowboys";

        Map<String,String> testMap = Maps.newLinkedHashMap();
        testMap.put("Washington D.C","Redskins");
        testMap.put("New York City","Giants");
        testMap.put("Philadelphia","Eagles");
        testMap.put("Dallas","Cowboys");
        String returnedString = Joiner.on("#").withKeyValueSeparator("=").join(testMap);

        assertThat(returnedString,is(expectedString));
    }

    @Test
    public void immutableTest() {
        Joiner stringJoiner = Joiner.on("|").skipNulls();

        // 不允许对同一类操作再进行指定，下面的代码会抛出异常 UnsupportedOperationException
        // stringJoiner.useForNull("missing");

        String result1 = stringJoiner.join(new String[]{"foo", "bar", null});
        System.out.println(result1);

        String result2 = stringJoiner.join(stringListWithNull);
        System.out.println(result2);

    }

    private String buildString(List<String> stringList, String delimiter) {
        StringBuilder builder = new StringBuilder();
        for (String s : stringList) {
            if (s != null) {
                builder.append(s).append(delimiter);
            }
        }

        builder.setLength(builder.length() - delimiter.length());
        return builder.toString();
    }

    @Test
    public void jdkStringDelimiterTest() {
        String result1 = buildString(stringListWithNull, delimiter);
        System.out.println(result1);
    }
}
