package org.xman.guava.basic;


import org.junit.Test;

import static com.google.common.base.Preconditions.*;

public class PreconditionsTest {

    /**
     * JDK中判断对象是否为空的通常做法。
     *
     * @param someObj 待测对象
     */
    public void jdkStyle(Object someObj) {
        if (someObj == null) {
            throw new IllegalArgumentException(" someObj must not be null");
        }
    }

    /**
     * Guava中判断对象是否为空的优雅做法。
     *
     * @param someObj 待测对象
     */
    public void guavaStyle(Object someObj) {
        checkNotNull(someObj, "someObj must not be null");
    }

    @Test
    public void testGuavaStyle() {
        Object obj = null;

        guavaStyle(obj);
    }

    /**
     * 检查数据序号是否越界，数值判断等。
     */
    @Test
    public void testCheckElementIndex() {
        int index = 2;
        int valueToSet = 888;
        int[] values = new int[5];

        // 检查序号是否合法
        int currentIndex = checkElementIndex(index, values.length, "序号已越界");

        // 赋值检查
        checkArgument(valueToSet <= 100, "数值不能超过100");
        values[currentIndex] = valueToSet;
    }

    /**
     * 状态检查的示例。
     */
    @Test
    public void testCheckState() {
        checkState(false, "状态异常，不能执行操作");

        System.out.println("一些操作");
    }

}
