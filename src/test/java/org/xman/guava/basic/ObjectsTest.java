package org.xman.guava.basic;


import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

public class ObjectsTest {

    class Book implements Comparable<Book> {
        private Person author;
        private String title;
        private String publisher;

        public Person getAuthor() {
            return author;
        }

        public void setAuthor(Person author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPublisher() {
            return publisher;
        }

        public void setPublisher(String publisher) {
            this.publisher = publisher;
        }


        @Override
        public int hashCode() {
            return Objects.hashCode(author, title, publisher);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            final Book other = (Book) obj;
            return Objects.equal(this.author, other.author) && Objects.equal(this.title, other.title) && Objects.equal(this.publisher, other.publisher);
        }

        @Override
        public int compareTo(Book o) {
            return ComparisonChain.start()
                    .compare(this.title, o.getTitle())
                    .compare(this.author, o.getAuthor())
                    .compare(this.publisher, o.getPublisher())
                    .result();
        }
    }

    class Person implements Comparable<Person> {
        private long id;
        private String name;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public int compareTo(Person o) {
            return ComparisonChain.start()
                    .compare(this.id, o.getId())
                    .compare(this.name, o.getName())
                    .result();
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(id, name);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            final Person other = (Person) obj;
            return Objects.equal(this.id, other.id) && Objects.equal(this.name, other.name);
        }
    }
}
