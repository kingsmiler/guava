package org.xman.guava.basic;


import com.google.common.base.Strings;
import org.junit.Test;

public class StringsTest {
    private String sequence = "foo";

    @Test
    public void testPadEnd() {
        String result = Strings.padEnd(sequence, 6, 'x');
        System.out.println(result);
    }

    @Test
    public void testPadStart() {
        String result = Strings.padStart(sequence, 6, 'x');
        System.out.println(result);
    }
}
