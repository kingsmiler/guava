package org.xman.guava.guide;


import com.google.common.base.Optional;
import com.google.common.base.Strings;
import org.junit.Assert;
import org.junit.Test;

public class AvoidingNullTest {

    @Test
    public void testOf() {
        Optional<Long> possible = Optional.of(5L);
        possible.get(); // returns 5
        Assert.assertEquals(true, possible.isPresent());

        possible = Optional.fromNullable(null);
        possible.absent();
    }

    @Test
    public void testGetOptional() {
        Optional.of(5);
        Optional.absent();
        Optional.fromNullable(null);
    }

    @Test
    public void testGetNull() {
        Strings.emptyToNull("aa");
        Strings.isNullOrEmpty("aa");
        Strings.nullToEmpty("aa");
    }
}
